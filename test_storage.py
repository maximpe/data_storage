from storage import *
from objects import *
import shutil as sh

# Тестирование работы с БД через Storage

original_db = "storage.sqlite"
test_db = "test.sqlite"


# Тест: Вставка, обновление и выбор объекта
def testCRUD(objClass, fields):
    print("START TEST: %s(%s)" % (sys._getframe().f_code.co_name, objClass.__name__))
    # db creating
    sh.copy(original_db, test_db)
    storage = Storage(test_db)
    storage.debug_print = True
    # object creating
    obj = objClass()
    obj.fromFields(fields, storage)
    assert (obj.id <= 0)
    # object insert and check
    storage.insert(obj)
    assert (obj.id > 0)
    # object select and check
    selected = storage.selectId(objClass, obj.id)
    assert (obj.id == selected.id)
    assert (obj.getValues() == selected.getValues())
    # object update, select and check
    # first_key = next(iter(fields))
    if "name" in fields:
        first_key = "name"
    elif "path" in fields:
        first_key = "path"
    elif "SN" in fields:
        first_key = "SN"
    elif "startTime" in fields:
        first_key = "startTime"
    elif "coordinates" in fields:
        first_key = "coordinates"
    setattr(obj, first_key, "updated")
    storage.update(obj)
    selected = storage.selectId(objClass, obj.id)
    assert (obj.id == selected.id)

    assert (obj.getValues() == selected.getValues())
    # insert second object and selectAll test
    storage.debug_print = False
    obj2 = objClass()
    obj2.fromFields(fields, storage)
    setattr(obj2, first_key, "second_object")
    storage.insert(obj2)
    #   В базе больше данных, проверка не актуальна
    '''objects = storage.selectAll(objClass)
    assert (len(objects) == 2)
    assert (obj.getValues() == objects[0].getValues())
    assert (obj2.getValues() == objects[1].getValues())'''


fields = {Season: {"name": "autumn", "title": "Осень", "descr": "Очей очарованье"},
          Place: {"name": "moscow", "title": "Москва", "descr": "Златоглавая"},
          Camera: {"SN": "1235D2S", "resolution": "700:900", "alias": "cam_1", "position": "top", "distorted": "20%", "focus": "auto"},
          Role: {"name": "рядовой разметчик", "title": "Рядовой", "descr": "размечает фотографии"},
          Label: {"name": "car", "title": "Машина", "descr": "обычная машина"},
          TaskStatus: {"name": "done", "title": "Выполнено", "descr": "задача выполнена"},
          Weather: {"name": "snow", "title": "Снег", "descr": "Снежная погода"},
          Condition: {"name": "morning", "title": "Утро", "descr": "Раннее утро"},
          Car: {"name": "car1", "title": "Машина1", "descr": "Классная машина 1"},
          User: {"name": "Ekaterina", "surname": "Sapegina", "startTime": "9:00", "endTime": "18:00"},
          Object: {"coordinates": "1:2:3"},
          Task: {"startTime": "2012-12-10", "endTime": "2012-12-20"},
          Image: {"hash": "153467F", "path": "D:/record1/video1/catalog1/image1.jpg"},
          Catalog: {"path": "D:/record1/video1/catalog1/", "duration": 316, "dateTime": "утро"},
          Record: {"path": "D:/record1/", "startTime": "14:30", "endTime": "17:30", "cameraPosition": "top"},
          Video: {"path": "D:/record1/video1/", "hash": "Лето", "duration": "Летний сезон"}
          }

for objClass in fields:
    print("-----------------")
    testCRUD(objClass, fields[objClass])
    print("OK")


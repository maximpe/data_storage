import sqlite3 as lite
import os

class Storage:
    def __init__(self, dbfile=""):
        self.debug_print = False
        self.dbfile = dbfile
        self.seasons = []
        self.places = []
        self.cameras = []
        # self.roles = [{"id": 1, "name": "рядовой разметчик", "title": "Рядовой", "descr": "размечает фотографии"}]
        self.roles = []
        self.labels = []
        self.taskstatuses = []
        self.weathers = []
        self.conditions = []
        self.cars = []

    def insert(self, obj):
        tablename = type(obj).tablename
        cmd = "INSERT INTO %s(" % tablename
        fields = type(obj).fields
        values = obj.getValues()
        assert (len(fields) > 0)
        assert (len(fields) == len(values))
        for key in fields:
            cmd += "%s," % key
        cmd = cmd[:-1] + ')'
        cmd += " VALUES("
        for key in fields:
            if type(values[key]) == type(""):
                cmd += "'%s'," % str(values[key])
            else:
                cmd += "%s," % str(values[key])
        cmd = cmd[:-1] + ')'
        if self.debug_print:
            print(cmd)
        obj.id = self.command(cmd)

    def update(self, obj):
        tablename = type(obj).tablename
        cmd = "UPDATE %s SET " % tablename
        fields = type(obj).fields
        values = obj.getValues()
        assert (len(fields) > 0)
        assert (len(fields) == len(values))
        for key in fields:
            if type(values[key]) == type(""):
                cmd += "%s='%s'," % (key, str(values[key]))
            else:
                cmd += "%s=%s," % (key, str(values[key]))
        cmd = cmd[:-1] + ' WHERE id==%i' % obj.id
        if self.debug_print:
            print(cmd)
        self.command(cmd)

    def selectId(self, objectClass, id):
        tablename = objectClass.tablename
        fields = objectClass.fields
        cmd = "SELECT * FROM %s WHERE id==%i" % (tablename, id)
        if len(self.command(cmd)) != 0:
            selected = self.command(cmd)[0]
            if self.debug_print:
                print(cmd)
            result = objectClass()
            result.fromFields(selected, self)
            result.id = int(selected["id"])
            return result

    def selectAll(self, objectClass):
        tablename = objectClass.tablename
        fields = objectClass.fields
        cmd = "SELECT * FROM %s" % (tablename)
        selected = self.command(cmd)
        if self.debug_print:
            print(cmd)
        result = []
        for row in selected:
            obj = objectClass()
            obj.fromFields(row, self)
            obj.id = int(row["id"])
            result.append(obj)
        return result

    def selectRange(self, objectClass, start, n):
        tablename = objectClass.tablename
        fields = objectClass.fields
        cmd = "SELECT * FROM %s" % (tablename)
        cmd += " WHERE id BETWEEN " + str(start) + " AND " + str(n)
        selected = self.command(cmd)
        if self.debug_print:
            print(cmd)
        result = []
        for row in selected:
            obj = objectClass()
            obj.fromFields(row, self)
            obj.id = int(row["id"])
            result.append(obj)
        return result

    def command(self, cmd):
        # Creates or opens a file called mydb with a SQLite3 DB
        is_insert = "insert" in cmd.lower()
        is_select = "select" in cmd.lower()
        db = lite.connect(self.dbfile)
        cursor = db.cursor()
        cursor.execute(cmd)
        if is_insert:
            id = cursor.lastrowid
        elif is_select:
            # values = cursor.fetchone()
            names = [description[0] for description in cursor.description]
            values = []
            for row in cursor:
                values.append(row)
        db.commit()
        db.close()
        if is_insert:
            return id
        elif is_select:
            result = []
            for row in values:
                res = dict(zip(names, row))
                result.append(res)
            return result


# Unit test
if __name__ == '__main__':
    pass

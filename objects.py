import sqlite3 as lite
import sys
from storage import Storage

class DBObject:
    tablename = ""
    fields = {}
    links = {}

    def __init__(self):
        self.id = -1
        for key in self.fields:
            if key in self.links:
                continue
            setattr(self, key, None)
        for link in self.links:
            setattr(self, self.links[link], None)

    def fromFields(self, fields, storage):
        for key in fields:
            if key in self.links:
                if fields[key] > 0:
                    if key == "idWorker" or key == "idAuthor":
                        obj = storage.selectId(eval("User"), fields[key])
                    elif key == "idStatus":
                        obj = storage.selectId(eval("TaskStatus"), fields[key])
                    elif key == "idParent":
                        if fields[key] != self.id:
                            obj = storage.selectId(eval("Place"), fields[key])
                        else:
                            obj = self
                    else:
                        name = key[2:]
                        obj = storage.selectId(eval(name), fields[key])
                    setattr(self, self.links[key], obj)
            setattr(self, key, fields[key])

    # Возвращает словарь поле-значение для записи в БД
    def getValues(self):
        result = {}
        for key in self.fields:
            if key in self.links:
                obj = getattr(self, self.links[key])
                if obj == None:
                    result[key] = -1
                else:
                    result[key] = obj.id
                continue
            result[key] = getattr(self, key)
        return result

#   классы-словари

class Season(DBObject):
    tablename = "seasons"
    fields = {"name", "title", "descr"}
    links = {}
    def __init__(self):
        super().__init__()

    def __str__(self):
        return self.title

    @staticmethod
    def test():
        season = Season()
        assert(type(season).tablename == Season.tablename)
        fields = {"name": "summer", "title": "Лето", "descr": "Летний сезон"}
        season.fromFields(fields, Storage())
        assert(season.getValues() == fields)
        assert(str(season) == fields["title"])
        print("%s::%s passed OK"%(__class__.__name__, sys._getframe().f_code.co_name))


class Place(DBObject):
    tablename = "places"
    fields = {"idParent", "name", "title", "descr"}
    links = {"idParent":"parent"}
    def __init__(self):
        super().__init__()

    def __str__(self):
        return self.title

    @staticmethod
    def test():
        place = Place()
        assert (type(place).tablename == Place.tablename)
        fields = {"idParent":-1, "name": "Moscow", "title": "Москва", "descr": "НАМИ"}
        place.fromFields(fields, Storage())
        assert(place.getValues() == fields)
        assert (str(place) == fields["title"])
        print("%s::%s passed OK" % (__class__.__name__, sys._getframe().f_code.co_name))


class Camera(DBObject):
    tablename = "cameras"
    fields = {"SN", "resolution", "alias", "position", "distorted", "focus"}
    links = {}
    def __init__(self):
        super().__init__()

#   У камеры метод str отдаёт серийный номер
    def __str__(self):
        return self.SN

    @staticmethod
    def test():
        camera = Camera()
        assert (type(camera).tablename == Camera.tablename)
        fields = {"SN": "1235D2S", "resolution": "700:900", "alias": "cam_1", "position": "top", "distorted": "20%", "focus": "auto"}
        camera.fromFields(fields, Storage())
        assert (camera.getValues() == fields)
        assert (str(camera) == fields["SN"])
        print("%s::%s passed OK" % (__class__.__name__, sys._getframe().f_code.co_name))


class Role(DBObject):
    tablename = "roles"
    fields = {"name", "title", "descr"}
    links = {}
    def __init__(self):
        super().__init__()

    def __str__(self):
        return self.title

    @staticmethod
    def test():
        role = Role()
        assert (type(role).tablename == Role.tablename)
        fields = {"name": "рядовой разметчик", "title": "Рядовой", "descr": "размечает фотографии"}
        role.fromFields(fields, Storage())
        assert (role.getValues() == fields)
        assert (str(role) == fields["title"])
        print("%s::%s passed OK" % (__class__.__name__, sys._getframe().f_code.co_name))


class Label(DBObject):
    tablename = "labels"
    fields = {"name", "title", "descr"}
    links = {}
    def __init__(self):
        super().__init__()

    def __str__(self):
        return self.title

    @staticmethod
    def test():
        label = Label()
        assert (type(label).tablename == Label.tablename)
        fields = {"name": "car", "title": "Машина", "descr": "обычная машина"}
        label.fromFields(fields, Storage())
        assert (label.getValues() == fields)
        assert (str(label) == fields["title"])
        print("%s::%s passed OK" % (__class__.__name__, sys._getframe().f_code.co_name))


class TaskStatus(DBObject):
    tablename = "taskstatuses"
    fields = {"name", "title", "descr"}
    links = {}
    def __init__(self):
        super().__init__()

    def __str__(self):
        return self.title

    @staticmethod
    def test():
        taskStatus = TaskStatus()
        assert (type(taskStatus).tablename == TaskStatus.tablename)
        fields = {"name": "done", "title": "Выполнено", "descr": "задача выполнена"}
        taskStatus.fromFields(fields, Storage())
        assert (taskStatus.getValues() == fields)
        assert (str(taskStatus) == fields["title"])
        print("%s::%s passed OK" % (__class__.__name__, sys._getframe().f_code.co_name))


class Weather(DBObject):
    tablename = "weathers"
    fields = {"name", "title", "descr"}
    links = {}
    def __init__(self):
        super().__init__()

    def __str__(self):
        return self.title

    @staticmethod
    def test():
        weather = Weather()
        assert (type(weather).tablename == Weather.tablename)
        fields = {"name": "snow", "title": "Снег", "descr": "Снежная погода"}
        weather.fromFields(fields, Storage())
        assert (weather.getValues() == fields)
        assert (str(weather) == fields["title"])
        print("%s::%s passed OK" % (__class__.__name__, sys._getframe().f_code.co_name))


class Condition(DBObject):
    tablename = "conditions"
    fields = {"name", "title", "descr"}
    links = {}
    def __init__(self):
        super().__init__()

    def __str__(self):
        return self.title

    @staticmethod
    def test():
        condition = Condition()
        assert (type(condition).tablename == Condition.tablename)
        fields = {"name": "morning", "title": "Утро", "descr": "Раннее утро"}
        condition.fromFields(fields, Storage())
        assert (condition.getValues() == fields)
        assert (str(condition) == fields["title"])
        print("%s::%s passed OK" % (__class__.__name__, sys._getframe().f_code.co_name))


class Car(DBObject):
    tablename = "cars"
    fields = {"name", "title", "descr"}
    links = {}
    def __init__(self):
        super().__init__()

    def __str__(self):
        return self.title

    @staticmethod
    def test():
        car = Car()
        assert (type(car).tablename == Car.tablename)
        fields = {"name": "car1", "title": "Машина1", "descr": "Классная машина 1"}
        car.fromFields(fields, Storage())
        assert (car.getValues() == fields)
        assert (str(car) == fields["title"])
        print("%s::%s passed OK" % (__class__.__name__, sys._getframe().f_code.co_name))


#  --------------------------------------------------------------------
#   классы-несловари

class User(DBObject):
    tablename = "users"
    fields = {"idRole", "name", "surname", "startTime", "endTime"}
    links = {"idRole": "role"}
    def __init__(self):
        super().__init__()

#   у пользователя строка является именем и фамилией
    def __str__(self):
        return self.name + " " + self.surname

    @staticmethod
    def test():
        user = User()
        assert (type(user).tablename == User.tablename)
        fields = {"idRole": -1, "name": "Ekaterina", "surname": "Sapegina", "startTime": "9:00", "endTime": "18:00"}
        user.fromFields(fields, Storage())
        assert (user.getValues() == fields)
        assert (str(user) == fields["name"] + " " + fields["surname"])
        print("%s::%s passed OK" % (__class__.__name__, sys._getframe().f_code.co_name))


class Object(DBObject):
    tablename = "objects"
    fields = {"idLabel", "idWorker", "idImage", "coordinates"}
    links = {"idLabel": "label", "idWorker": "worker", "idImage": "image"}
    def __init__(self):
        super().__init__()

    def __str__(self):
        return str(self.coordinates)

    @staticmethod
    def test():
        object = Object()
        assert (type(object).tablename == Object.tablename)
        fields = {"idLabel": -1, "idWorker": -1, "idImage": -1, "coordinates": "1:2:3"}
        object.fromFields(fields, Storage())
        assert (object.getValues() == fields)
        assert (str(object) == str(fields["coordinates"]))
        print("%s::%s passed OK" % (__class__.__name__, sys._getframe().f_code.co_name))


class Task(DBObject):
    tablename = "tasks"
    fields = {"idStatus", "idCatalog", "idWorker", "idAuthor", "startTime",
              "endTime"}
    links = {"idStatus": "taskStatus", "idCatalog": "catalog", "idWorker": "worker", "idAuthor": "author"}
    def __init__(self):
        super().__init__()

#   у задачи строка описывает id работника плюс начало и конец выполнения
    def __str__(self):
        return str(self.idWorker) + " " + self.startTime + " " + self.endTime

    @staticmethod
    def test():
        task = Task()
        assert (type(task).tablename == Task.tablename)
        fields = {"idStatus": -1, "idCatalog": -1, "idWorker": -1, "idAuthor": -1, "startTime": "2012-12-10", "endTime": "2012-12-10"}
        task.fromFields(fields, Storage())
        assert (task.getValues() == fields)
        assert (str(task) == str(fields["idWorker"]) + " " + fields["startTime"] + " " + fields["endTime"])
        print("%s::%s passed OK" % (__class__.__name__, sys._getframe().f_code.co_name))


class Image(DBObject):
    tablename = "images"
    fields = {"idCatalog", "hash", "path"}
    links = {"idCatalog": "catalog"}
    def __init__(self):
        super().__init__()

#   у картинки строка выдаёт путь к ней
    def __str__(self):
        return self.path

    @staticmethod
    def test():
        image = Image()
        assert (type(image).tablename == Image.tablename)
        fields = {"idCatalog": -1, "hash": "153467F", "path": "D:/record1/video1/catalog1/image1.jpg"}
        image.fromFields(fields, Storage())
        assert (image.getValues() == fields)
        assert (str(image) == fields["path"])
        print("%s::%s passed OK" % (__class__.__name__, sys._getframe().f_code.co_name))


class Catalog(DBObject):
    tablename = "catalogs"
    fields = {"idVideo", "path", "duration", "dateTime"}
    links = {"idVideo": "video"}
    def __init__(self):
        super().__init__()

#   у каталога строка выдаёт путь к нему
    def __str__(self):
        return self.path

    @staticmethod
    def test():
        catalog = Catalog()
        assert (type(catalog).tablename == Catalog.tablename)
        fields = {"idVideo": -1, "path": "D:/record1/video1/catalog1/", "duration": 316, "dateTime": "утро"}
        catalog.fromFields(fields, Storage())
        assert (catalog.getValues() == fields)
        assert (str(catalog) == fields["path"])
        print("%s::%s passed OK" % (__class__.__name__, sys._getframe().f_code.co_name))



class Record(DBObject):
    tablename = "records"
    fields = {"idPlace", "idCamera", "idSeason", "idCar", "path", "startTime", "endTime", "cameraPosition"}
    links = {"idPlace": "place", "idCamera": "camera", "idSeason": "season", "idCar": "car"}
    def __init__(self):
        super().__init__()

#   у записи строка выдаёт путь к ней
    def __str__(self):
        return self.path

    @staticmethod
    def test():
        record = Record()
        assert (type(record).tablename == Record.tablename)
        fields = {"idPlace": -1, "idCamera": -1, "idSeason": -1, "idCar": -1, "path": "D:/record1/", "startTime": "14:30", "endTime": "17:30", "cameraPosition": "top"}
        record.fromFields(fields, Storage())
        assert (record.getValues() == fields)
        assert (str(record) == fields["path"])
        print("%s::%s passed OK" % (__class__.__name__, sys._getframe().f_code.co_name))


class Video(DBObject):
    tablename = "videos"
    fields = {"idRecord", "idCondition", "idWeather", "path", "hash",
              "duration"}
    links = {"idRecord": "record", "idCondition": "condition", "idWeather": "weather"}
    def __init__(self):
        super().__init__()

#   у видео строка выдаёт путь к нему
    def __str__(self):
        return self.path

    @staticmethod
    def test():
        video = Video()
        assert (type(video).tablename == Video.tablename)
        fields = {"idRecord": -1, "idCondition": -1, "idWeather": -1, "path": "D:/record1/video1/", "hash": "Лето", "duration": "Летний сезон"}
        video.fromFields(fields, Storage())
        assert (video.getValues() == fields)
        assert (str(video) == fields["path"])
        print("%s::%s passed OK" % (__class__.__name__, sys._getframe().f_code.co_name))


# Unit test
if __name__ == '__main__':
    Season.test()
    Place.test()
    Camera.test()
    Role.test()
    Label.test()
    TaskStatus.test()
    Weather.test()
    Condition.test()
    Car.test()
    User.test()
    Object.test()
    Task.test()
    Image.test()
    Catalog.test()
    Record.test()
    Video.test()

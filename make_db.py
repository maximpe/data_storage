import sqlite3 as lite

con = lite.connect('./storage.sqlite')

with con:
    cur = con.cursor()

    # Включаем поддержку внешних ключей
    cur.execute("PRAGMA foreign_keys=on;")


class Field:
    def __init__(self, name, type):
        self.name = name
        self.type = type
        self.fieldType = 'Field'


class Reference:
    def __init__(self, childField, parentTable, parentField):
        self.parentTable = parentTable
        self.childField = childField
        self.parentField = parentField
        self.fieldType = 'Reference'


# Tables schemas
tables = {
    'places': [
        Field('id', 'INTEGER PRIMARY KEY')
        , Field('idParent', 'INTEGER NOT NULL')
        , Field('name', 'TEXT NOT NULL')
        , Field('title', 'TEXT NOT NULL')
        , Field('descr', 'TEXT NOT NULL')
        , Reference('idParent', 'places', 'id')
    ],
    'cameras': [
        Field('id', 'INTEGER PRIMARY KEY')
        , Field('SN', 'TEXT NOT NULL')
        , Field('resolution', 'TEXT NOT NULL')
        , Field('alias', 'TEXT NOT NULL')
        , Field('position', 'TEXT NOT NULL')
        , Field('distorted', 'INTEGER NOT NULL')
        , Field('focus', 'REAL NOT NULL')
    ],
    'roles': [
        Field('id', 'INTEGER PRIMARY KEY')
        , Field('name', 'TEXT NOT NULL')
        , Field('title', 'TEXT NOT NULL')
        , Field('descr', 'TEXT NOT NULL')
    ],
    'users': [
        Field('id', 'INTEGER PRIMARY KEY')
        , Field('idRole', 'INTEGER NOT NULL')
        , Field('name', 'TEXT NOT NULL')
        , Field('surname', 'TEXT NOT NULL')
        , Field('startTime', 'DATE NOT NULL')
        , Field('endTime', 'DATE NOT NULL')
        , Reference('idRole', 'roles', 'id')
    ],
    'labels': [
        Field('id', 'INTEGER PRIMARY KEY')
        , Field('name', 'TEXT NOT NULL')
        , Field('title', 'TEXT NOT NULL')
        , Field('descr', 'TEXT NOT NULL')
    ],
    'objects': [
        Field('id', 'INTEGER PRIMARY KEY')
        , Field('idLabel', 'INTEGER NOT NULL')
        , Field('idWorker', 'INTEGER NOT NULL')
        , Field('idImage', 'INTEGER NOT NULL')
        , Field('coordinates', 'TEXT NOT NULL')
        , Reference('idLabel', 'labels', 'id')
        , Reference('idWorker', 'users', 'id')
        , Reference('idImage', 'images', 'id')
    ],
    'taskStatuses': [
        Field('id', 'INTEGER PRIMARY KEY')
        , Field('name', 'TEXT NOT NULL')
        , Field('title', 'TEXT NOT NULL')
        , Field('descr', 'TEXT NOT NULL')
    ],
    'tasks': [
        Field('id', 'INTEGER PRIMARY KEY')
        , Field('idStatus', 'INTEGER NOT NULL')
        , Field('idWorker', 'INTEGER NOT NULL')
        , Field('idCatalog', 'INTEGER NOT NULL')
        , Field('idAuthor', 'INTEGER NOT NULL')
        , Field('startTime', 'DATETIME NOT NULL')
        , Field('endTime', 'DATETIME NOT NULL')
        , Reference('idStatus', 'taskStatuses', 'id')
        , Reference('idWorker', 'users', 'id')
        , Reference('idCatalog', 'catalogs', 'id')
        , Reference('idAuthor', 'users', 'id')
    ],
    'images': [
        Field('id', 'INTEGER PRIMARY KEY')
        , Field('idCatalog', 'INTEGER NOT NULL')
        , Field('hash', 'TEXT NOT NULL')
        , Field('path', 'TEXT NOT NULL')
        , Reference('idCatalog', 'catalogs', 'id')
    ],
    'catalogs': [
        Field('id', 'INTEGER PRIMARY KEY')
        , Field('idVideo', 'INTEGER NOT NULL')
        , Field('path', 'TEXT NOT NULL')
        , Field('duration', 'INTEGER NOT NULL')
        , Field('dateTime', 'DATETIME NOT NULL')
        , Reference('idVideo', 'videos', 'id')
    ],
    'weathers': [
        Field('id', 'INTEGER PRIMARY KEY')
        , Field('name', 'TEXT NOT NULL')
        , Field('title', 'TEXT NOT NULL')
        , Field('descr', 'TEXT NOT NULL')
    ],
    'conditions': [
        Field('id', 'INTEGER PRIMARY KEY')
        , Field('name', 'TEXT NOT NULL')
        , Field('title', 'TEXT NOT NULL')
        , Field('descr', 'TEXT NOT NULL')
    ],
    'seasons': [
        Field('id', 'INTEGER PRIMARY KEY')
        , Field('name', 'TEXT NOT NULL')
        , Field('title', 'TEXT NOT NULL')
        , Field('descr', 'TEXT NOT NULL')
    ],
    'cars': [
        Field('id', 'INTEGER PRIMARY KEY')
        , Field('name', 'TEXT NOT NULL')
        , Field('title', 'TEXT NOT NULL')
        , Field('descr', 'TEXT NOT NULL')
    ],
    'records': [
        Field('id', 'INTEGER PRIMARY KEY')
        , Field('idPlace', 'INTEGER NOT NULL')
        , Field('idCamera', 'INTEGER NOT NULL')
        , Field('idSeason', 'INTEGER NOT NULL')
        , Field('idCar', 'INTEGER NOT NULL')
        , Field('path', 'TEXT NOT NULL')
        , Field('startTime', 'DATETIME NOT NULL')
        , Field('endTime', 'DATETIME NOT NULL')
        , Field('cameraPosition', 'TEXT NOT NULL')
        , Reference('idPlace', 'places', 'id')
        , Reference('idCamera', 'cameras', 'id')
        , Reference('idSeason', 'seasons', 'id')
        , Reference('idCar', 'cars', 'id')
    ],
    'videos': [
        Field('id', 'INTEGER PRIMARY KEY')
        , Field('idRecord', 'INTEGER NOT NULL')
        , Field('idCondition', 'INTEGER NOT NULL')
        , Field('idWeather', 'INTEGER NOT NULL')
        , Field('path', 'TEXT NOT NULL')
        , Field('hash', 'TEXT NOT NULL')
        , Field('duration', 'INTEGER NOT NULL')
        , Reference('idRecord', 'records', 'id')
        , Reference('idCondition', 'conditions', 'id')
        , Reference('idWeather', 'weathers', 'id')
    ],
}

# Creating tables
for table in tables.keys():
    try:
        cmd = 'CREATE TABLE %s(' % table
        for field in tables[table]:
            if field.fieldType == 'Field':
                cmd += '%s %s,' % (field.name, field.type)
            else:
                cmd += ' FOREIGN KEY (%s) REFERENCES %s(%s),' % (field.childField, field.parentTable, field.parentField)
        cmd = cmd[:-1] + ')'
        print(cmd)
        cur.execute(cmd)
    except:
        pass

def insert_data():
    insert_places()
    insert_cameras()
    insert_roles()
    insert_users()
    insert_labels()
    insert_taskStatuses()
    insert_weathers()
    insert_conditions()
    insert_seasons()
    insert_cars()
    insert_records()
    insert_videos()
    insert_catalogs()
    insert_tasks()
    insert_images()
    insert_objects()


def insert_places():
    cmd = "INSERT INTO places VALUES (1, 1, 'москва', 'москва', 'столица россии'), \
                                    (2, 1, 'север москвы', 'москва', 'полигон'), \
                                    (3, 1, 'юг москвы', 'москва', 'полигон'), \
                                    (4, 1, 'запад москвы', 'москва', 'полигон'), \
                                    (5, 1, 'север москвы', 'москва', 'полигон'), \
                                        (6, 1, 'юг москвы', 'москва', 'полигон'), \
                                        (7, 1, 'запад москвы', 'москва', 'полигон'), \
                                        (8, 1, 'восток москвы', 'москва', 'полигон'), \
                                        (9, 1, 'север москвы', 'москва', 'полигон'), \
                                        (10, 1, 'юг москвы', 'москва', 'полигон'), \
                                        (11, 1, 'запад москвы', 'москва', 'полигон'), \
                                        (12, 1, 'восток москвы', 'москва', 'полигон'), \
                                        (13, 1, 'север москвы', 'москва', 'полигон'), \
                                        (14, 1, 'юг москвы', 'москва', 'полигон'), \
                                        (15, 1, 'запад москвы', 'москва', 'полигон'), \
                                        (16, 1, 'восток москвы', 'москва', 'полигон'), \
                                        (17, 1, 'север москвы', 'москва', 'полигон'), \
                                        (18, 1, 'юг москвы', 'москва', 'полигон'), \
                                        (19, 1, 'запад москвы', 'москва', 'полигон'), \
                                        (20, 1, 'восток москвы', 'москва', 'полигон'), \
                                        (21, 1, 'север москвы', 'москва', 'полигон'), \
                                        (22, 1, 'юг москвы', 'москва', 'полигон'), \
                                        (23, 1, 'запад москвы', 'москва', 'полигон'), \
                                        (24, 1, 'восток москвы', 'москва', 'полигон'), \
                                        (25, 1, 'север москвы', 'москва', 'полигон'), \
                                        (26, 1, 'юг москвы', 'москва', 'полигон'), \
                                        (27, 1, 'запад москвы', 'москва', 'полигон'), \
                                        (28, 1, 'восток москвы', 'москва', 'полигон'), \
                                        (29, 1, 'запад москвы', 'москва', 'полигон'), \
                                            (30, 1, 'восток москвы', 'москва', 'полигон'), \
                                    (31, 1, 'восток москвы', 'москва', 'полигон')"
    cur.execute(cmd)
    con.commit()


def insert_cameras():
    cmd = "INSERT INTO cameras VALUES (1, '1235D2S', '700:900', 'cam_1', 'top', '20%', 'auto'), \
                                    (2, 'THDS4', '700:900', 'cam_2', 'top', '20%', 'auto'), \
                                    (3, 'RSYJNRS215F', '500:700', 'cam_3', 'top', '20%', 'auto'), \
                                    (4, 'DH785GD', '1800:720', 'cam_4', 'top', '20%', 'auto'), \
                                    (5, 'DETHT67DGF', '700:900', 'cam_5', 'top', '20%', 'auto')"
    cur.execute(cmd)
    con.commit()


def insert_roles():
    cmd = "INSERT INTO roles VALUES (1, 'рядовой разметчик', 'рядовой', 'размечает фотографии'), \
                                    (2, 'программист сети', 'программист', 'программирует сети'), \
                                    (3, 'администратор базы', 'администратор', 'администрирует базу')"
    cur.execute(cmd)
    con.commit()


def insert_users():
    cmd = "INSERT INTO users VALUES (1, 1, 'екатерина', 'сапегина', '19-01-01', '19-02-01'), \
                                    (2, 1, 'дарья', 'иванова', '19-01-01', '19-03-01'), \
                                    (3, 2, 'сергей', 'петров', '19-01-01', '19-04-01'), \
                                    (4, 2, 'рашид', 'мейланов', '19-01-01', '19-05-01'), \
                                    (5, 3, 'иван', 'иванов', '19-01-01', '19-06-01')"
    cur.execute(cmd)
    con.commit()


def insert_labels():
    cmd = "INSERT INTO labels VALUES (1, 'car', 'машина', 'обычная машина'), \
                                    (2, 'person', 'человек', 'обычный человек'), \
                                    (3, 'bicycle', 'велосипед', 'обычный велосипед')"
    cur.execute(cmd)
    con.commit()


def insert_objects():
    cmd = "INSERT INTO objects VALUES (1, 1, 1, 1, '1:2:3'), \
                                    (2, 1, 2, 1, '1:2:3'), \
                                    (3, 2, 3, 1, '1:2:3'), \
                                    (4, 2, 4, 2, '1:2:3'), \
                                    (5, 3, 5, 3, '1:2:3')"
    cur.execute(cmd)
    con.commit()


def insert_taskStatuses():
    cmd = "INSERT INTO taskStatuses VALUES (1, 'done', 'выполнено', 'задача выполнена в полном объёме'), \
                                    (2, 'not done', 'не выполнено', 'задача не выполнена в полном объёме'), \
                                    (3, 'delayed', 'отложено', 'задача отложена'), \
                                    (4, 'canсelled', 'отменена', 'задача не будет выполнена')"
    cur.execute(cmd)
    con.commit()


def insert_tasks():
    cmd = "INSERT INTO tasks VALUES (1, 1, 1, 1, 3, '19-01-01 12:00', '19-01-01 14:00'), \
                                    (2, 1, 2, 2, 3, '19-01-01 12:00', '19-01-01 14:00'), \
                                    (3, 2, 3, 3, 4, '19-01-01 12:00', '19-01-01 14:00'), \
                                    (4, 3, 4, 4, 4, '19-01-01 12:00', '19-01-01 14:00'), \
                                    (5, 4, 5, 5, 4, '19-01-01 12:00', '19-01-01 14:00')"
    cur.execute(cmd)
    con.commit()


def insert_images():
    cmd = "INSERT INTO images VALUES (1, 1, '153467F', 'D:/record1/video1/catalog1/image1.jpg'), \
                                    (2, 2, 'GFNSS4', 'D:/record1/video1/catalog2/image2.jpg'), \
                                    (3, 3, 'RT5HBAFTH', 'D:/record1/video1/catalog3/image3.jpg'), \
                                    (4, 4, '54HFDHT5', 'D:/record1/video1/catalog4/image4.jpg'), \
                                    (5, 5, 'R5TH56HF', 'D:/record1/video1/catalog5/image5.jpg')"
    cur.execute(cmd)
    con.commit()


def insert_catalogs():
    cmd = "INSERT INTO catalogs VALUES (1, 1, 'D:/record1/video1/catalog1/', 316, '19-01-01 12:00'), \
                                    (2, 2, 'D:/record1/video2/catalog2/', 316, '19-01-01 12:00'), \
                                    (3, 3, 'D:/record1/video3/catalog3/', 316, '19-01-01 12:00'), \
                                    (4, 4, 'D:/record1/video4/catalog4/', 316, '19-01-01 12:00'), \
                                    (5, 5, 'D:/record1/video5/catalog5/', 316, '19-01-01 12:00')"
    cur.execute(cmd)
    con.commit()


def insert_weathers():
    cmd = "INSERT INTO weathers VALUES (1, 'snow', 'снег', 'снежная погода'), \
                                    (2, 'rain', 'дождь', 'дождливая погода'), \
                                    (3, 'sun', 'солнце', 'солничная погода'), \
                                    (4, 'fog', 'туман', 'туманная погода')"
    cur.execute(cmd)
    con.commit()


def insert_conditions():
    cmd = "INSERT INTO conditions VALUES (1, 'morning', 'утро', 'раннее утро'), \
                                    (2, 'evening', 'вечер', 'ранний вечер'), \
                                    (3, 'day', 'день', 'день'), \
                                    (4, 'night', 'ночь', 'поздняя ночь')"
    cur.execute(cmd)
    con.commit()


def insert_seasons():
    cmd = "INSERT INTO seasons VALUES (1, 'autumn', 'осень', 'очей очарованье'), \
                                    (2, 'winter', 'осень', 'очей очарованье'), \
                                    (3, 'spring', 'осень', 'очей очарованье'), \
                                    (4, 'summer', 'осень', 'очей очарованье')"
    cur.execute(cmd)
    con.commit()


def insert_cars():
    cmd = "INSERT INTO cars VALUES (1, 'марка машины_1', 'имя машины_1', 'классная машина1'), \
                                    (2, 'марка машины_2', 'имя машины_2', 'новая машина2'), \
                                    (3, 'марка машины_3', 'имя машины_3', 'старая машина3'), \
                                    (4, 'марка машины_4', 'имя машины_4', 'быстрая машина4'), \
                                    (5, 'марка машины_5', 'имя машины_5', 'сломанная машина5')"
    cur.execute(cmd)
    con.commit()


def insert_records():
    cmd = "INSERT INTO records VALUES (1, 2, 1, 1, 1, 'D:/record1/', '19-01-01 12:00', '19-01-01 18:00', 'top'), \
                                    (2, 2, 2, 2, 2, 'D:/record2/', '19-01-01 12:00', '19-02-01 18:00', 'top'), \
                                    (3, 3, 3, 3, 3, 'D:/record3/', '19-01-01 12:00', '19-03-01 18:00', 'top'), \
                                    (4, 4, 4, 3, 4, 'D:/record4/', '19-01-01 12:00', '19-04-01 18:00', 'top'), \
                                    (5, 5, 5, 4, 5, 'D:/record5/', '19-02-01 12:00', '19-04-01 18:00', 'top')"
    cur.execute(cmd)
    con.commit()


def insert_videos():
    cmd = "INSERT INTO videos VALUES (1, 1, 1, 1, 'D:/record1/video1/', 'LKN52LN', 42), \
                                    (2, 1, 2, 2, 'D:/record1/video2/', 'LKN52LN', 42), \
                                    (3, 1, 3, 2, 'D:/record1/video3/', 'LKN52LN', 42), \
                                    (4, 2, 3, 3, 'D:/record2/video4/', 'LKN52LN', 42), \
                                    (5, 3, 4, 4, 'D:/record3/video5/', 'LKN52LN', 42)"
    cur.execute(cmd)
    con.commit()


def test():
    cmd = "SELECT * FROM places"
    cur.execute(cmd)
    d = cur.fetchall()
    if not d:
        print("no data")
    else:
        print("places:")
        for x in d:
            print(x)
    cmd = "SELECT * FROM seasons"
    cur.execute(cmd)
    d = cur.fetchall()
    if not d:
        print("no data")
    else:
        print("seasons:")
        for x in d:
            print(x)
    cmd = "SELECT * FROM cameras"
    cur.execute(cmd)
    d = cur.fetchall()
    if not d:
        print("no data")
    else:
        print("cameras:")
        for x in d:
            print(x)
    cmd = "SELECT * FROM roles"
    cur.execute(cmd)
    d = cur.fetchall()
    if not d:
        print("no data")
    else:
        print("roles:")
        for x in d:
            print(x)
    cmd = "SELECT * FROM labels"
    cur.execute(cmd)
    d = cur.fetchall()
    if not d:
        print("no data")
    else:
        print("labels:")
        for x in d:
            print(x)
    cmd = "SELECT * FROM taskStatuses"
    cur.execute(cmd)
    d = cur.fetchall()
    if not d:
        print("no data")
    else:
        print("taskStatuses:")
        for x in d:
            print(x)
    cmd = "SELECT * FROM weathers"
    cur.execute(cmd)
    d = cur.fetchall()
    if not d:
        print("no data")
    else:
        print("weathers:")
        for x in d:
            print(x)
    cmd = "SELECT * FROM conditions"
    cur.execute(cmd)
    d = cur.fetchall()
    if not d:
        print("no data")
    else:
        print("conditions:")
        for x in d:
            print(x)
    cmd = "SELECT * FROM cars"
    cur.execute(cmd)
    d = cur.fetchall()
    if not d:
        print("no data")
    else:
        print("cars:")
        for x in d:
            print(x)
    cmd = "SELECT * FROM users"
    cur.execute(cmd)
    d = cur.fetchall()
    if not d:
        print("no data")
    else:
        print("users:")
        for x in d:
            print(x)
    cmd = "SELECT * FROM objects"
    cur.execute(cmd)
    d = cur.fetchall()
    if not d:
        print("no data")
    else:
        print("objects:")
        for x in d:
            print(x)
    cmd = "SELECT * FROM tasks"
    cur.execute(cmd)
    d = cur.fetchall()
    if not d:
        print("no data")
    else:
        print("tasks:")
        for x in d:
            print(x)
    cmd = "SELECT * FROM images"
    cur.execute(cmd)
    d = cur.fetchall()
    if not d:
        print("no data")
    else:
        print("images:")
        for x in d:
            print(x)
    cmd = "SELECT * FROM videos"
    cur.execute(cmd)
    d = cur.fetchall()
    if not d:
        print("no data")
    else:
        print("videos:")
        for x in d:
            print(x)
    cmd = "SELECT * FROM catalogs"
    cur.execute(cmd)
    d = cur.fetchall()
    if not d:
        print("no data")
    else:
        print("catalogs:")
        for x in d:
            print(x)
    cmd = "SELECT * FROM records"
    cur.execute(cmd)
    d = cur.fetchall()
    if not d:
        print("no data")
    else:
        print("records:")
        for x in d:
            print(x)
    


insert_data()
test()

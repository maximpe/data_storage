from flask import *
from objects import *
from storage import *

DBFILE = "storage.sqlite"
PAGE_SIZE = 20
app = Flask(__name__)

def out(obj, data):
    return render_template('base_output.html', obj=obj, data=data)
    
@app.route('/')
def start():
    return redirect(url_for('seasons'))

@app.route('/records')
def records():
    id = request.args.get('id')
    page = request.args.get('page')
    
    # Запрос данных
    storage = Storage(DBFILE)
    if id and not page:
        id = int(id)
        objects = [storage.selectId(Record, id)]
    elif page and not id:
        page = int(page)
        first = page*PAGE_SIZE
        last = first+PAGE_SIZE
        objects = storage.selectRange(Record, first, last)
    else:
        objects = storage.selectAll(Record)
    return out(Record, objects)

@app.route('/videos')
def videos():
    id = request.args.get('id')
    page = request.args.get('page')
    
    # Запрос данных
    storage = Storage(DBFILE)
    if id and not page:
        id = int(id)
        objects = [storage.selectId(Video, id)]
    elif page and not id:
        page = int(page)
        first = page*PAGE_SIZE
        last = first+PAGE_SIZE
        objects = storage.selectRange(Video, first, last)
    else:
        objects = storage.selectAll(Video)
    return out(Video, objects)

@app.route('/places')
def places():
    id = request.args.get('id')
    page = request.args.get('page')
    
    # Запрос данных
    storage = Storage(DBFILE)
    if id and not page:
        id = int(id)
        objects = [storage.selectId(Place, id)]
    elif page and not id:
        page = int(page)
        first = page*PAGE_SIZE
        last = first+PAGE_SIZE
        objects = storage.selectRange(Place, first, last)
    else:
        objects = storage.selectAll(Place)
    
    return out(Place, objects)

@app.route('/cameras')
def cameras():
    id = request.args.get('id')
    page = request.args.get('page')
    
    #Запрос данных
    storage = Storage(DBFILE)
    if id and not page:
        id = int(id)
        objects = [storage.selectId(Camera, id)]
    elif page and not id:
        page = int(page)
        first = page*PAGE_SIZE
        last = first+PAGE_SIZE
        objects = storage.selectRange(Camera, first, last)
    else:
        objects = storage.selectAll(Camera)
    
    return out(Camera, objects)

@app.route('/seasons')
def seasons():
    id = request.args.get('id')
    page = request.args.get('page')
    
    # Запрос данных
    storage = Storage(DBFILE)
    if id and not page:
        id = int(id)
        objects = [storage.selectId(Season, id)]
    elif page and not id:
        page = int(page)
        first = page*PAGE_SIZE
        last = first+PAGE_SIZE
        objects = storage.selectRange(Season, first, last)
    else:
        objects = storage.selectAll(Season)
    
    return out(Season, objects)

@app.route('/cars')
def cars():
    id = request.args.get('id')
    page = request.args.get('page')
    
    # Запрос данных
    storage = Storage(DBFILE)
    if id and not page:
        id = int(id)
        objects = [storage.selectId(Car, id)]
    elif page and not id:
        page = int(page)
        first = page*PAGE_SIZE
        last = first+PAGE_SIZE
        objects = storage.selectRange(Car, first, last)
    else:
        objects = storage.selectAll(Car)
    
    return out(Car, objects)

@app.route('/conditions')
def conditions():
    id = request.args.get('id')
    page = request.args.get('page')
    
    # Запрос данных
    storage = Storage(DBFILE)
    if id and not page:
        id = int(id)
        objects = [storage.selectId(Condition, id)]
    if page and not id:
        page = int(page)
        first = page*PAGE_SIZE
        last = first+PAGE_SIZE
        objects = storage.selectRange(Condition, first, last)
    else:
        objects = storage.selectAll(Condition)
    
    return out(Condition, objects)

@app.route('/weathers')
def weathers():
    id = request.args.get('id')
    page = request.args.get('page')
    
    # Запрос данных
    storage = Storage(DBFILE)
    if id and not page:
        id = int(id)
        objects = [storage.selectId(Weather, id)]
    if page and not id:
        page = int(page)
        first = page*PAGE_SIZE
        last = first+PAGE_SIZE
        objects = storage.selectRange(Weather, first, last)
    else:
        objects = storage.selectAll(Weather)
    
    return out(Weather, objects)




    
    
    
    
